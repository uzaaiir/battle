﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace battle
{
    public class ship
    {
        public ship()
        {
            sPoints = new List<points>();

        }
        public int Id { get; set; }
        public string name { get; set; }

        public string type { get; set; }

        public List<points> sPoints { get; set; }

        public int hits { get; set; }
    }
    public class points
    {
        public int x { get; set; }
        public int y { get; set; }

        public bool hit { get; set; }
    }
}
