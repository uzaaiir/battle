﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace battle
{
    class Program
    {
        static string[,] board = new string[10, 10];
        static List<ship> ships = new List<ship>();
        enum rows { A, B, C, D, E, F, G,H,I,J};
        static void Main(string[] args)
        {
            Console.WriteLine("hello world!");
            int x = (int)rows.B;
            for (var i = 0; i < 2; i++)
            {
                drawShip("S", "BattleShip" + i);
            }
            for (var i = 0; i < 1; i++)
            {
                drawShip("D", "Destroyer" + i);
            }

            drawBoard(board);
            Console.Write("Target at :");
            int var = 0;

            while (true)
            {
                try
                {
                    string input = Console.ReadLine();
                    string sRow = Regex.Match(input, @"[A-Za-z]").ToString();
                    int col = Convert.ToInt32(Regex.Match(input, @"\d+").ToString());
                    int row = (int)Enum.Parse(typeof(rows), sRow.ToUpper());

                    string s = isHit(row, col);

                    Console.WriteLine(s);
                }
                catch (Exception)
                {
                    Console.WriteLine("Wrong Input");
                    Console.Write("Target at :");
                }
            }
            //if(s!=null && s.name!= "" )
            //{
            //    Console.WriteLine("Ship hit" + s.name);
            //}

            
        }

        #region functions

        static void drawBoard(string[,] board)
        {
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    if((string.IsNullOrEmpty(board[i,j]))) // If Not Updated  For Start Case Only
                    {
                        board[i, j] = "   |";
                    }
                    Console.Write(board[i, j]);
                }
                Console.WriteLine();
            }
        }
        private static void drawShip(string type, string name)
        {
           
            ship s = new ship();
            Random rand = new Random();
            int x = rand.Next(0, 9);
            int y = rand.Next(0, 9);
            Console.WriteLine("{0} and {1}", x, y);

            bool check = false;
            bool drawn = false;

            
            int limit = type == "S" ? 5 : 4;

            if (!isLeft(y))
            {
                for (int i = 0; i < limit; i++)
                {
                    if (board[x, y + i] == "   |" || string.IsNullOrEmpty(board[x, y + i]))
                    {
                        check = true;
                    }
                    else
                    {
                        check = false;
                        break;
                    }
                }
                if (check)
                {
                    for (int i = 0; i < limit; i++)
                    {
                        points p = new points();
                        p.x = x;
                        p.y = y + i;
                        s.sPoints.Add(p);
                        board[x, y + i] = " "+type+" |";
                        drawn = true;
                    }
                }
            }
            else
            {
                for (int i = 0; i < limit; i++)
                {
                    if (board[x, y - i] == "   |" || string.IsNullOrEmpty(board[x, y - i]))
                    {
                        check = true;
                    }
                    else
                    {
                        check = false;
                        break;
                    }

                }
                if (check)
                {
                    for (int i = 0; i < limit; i++)
                    {
                        points p = new points();
                        p.x = x;
                        p.y = y - i;
                        s.sPoints.Add(p);
                        board[x, y - i] = " "+type+" |";
                        drawn = true;
                    }
                }
            }

            if (!drawn)
            {
                check = false;
                if (!isUp(x))
                {
                    for (int i = 0; i < limit; i++)
                    {
                        if (board[x + i, y] == "   |" || string.IsNullOrEmpty(board[x + i, y]))
                        {
                            check = true;
                        }
                        else
                        {
                            check = false;
                            break;
                        }
                    }
                    if (check)
                    {
                        for (int i = 0; i < limit; i++)
                        {
                            points p = new points();
                            p.x = x;
                            p.y = y - i;
                            s.sPoints.Add(p);
                            board[x + i, y] = " "+type+" |";

                        } 
                    }
                }
                else
                {
                    for (int i = 0; i < limit; i++)
                    {
                        if (board[x - i, y] == "   |" || string.IsNullOrEmpty(board[x - i, y]))
                        {
                            check = true;
                        }
                        else
                        {
                            check = false;
                            break;
                        }
                    }
                    if (check)
                    {
                        for (int i = 0; i < limit; i++)
                        {
                            points p = new points();
                            p.x = x;
                            p.y = y - i;
                            s.sPoints.Add(p);
                            board[x - i, y] = " "+type+" |";
                        } 
                    }
                }
            }
            s.name = name;
            ships.Add(s);
        }

        static bool isLeft(int y)
        {
            if(board.GetLength(0)-y > 5)
            {
                return false;
            }
            return true;
        }
        static bool isUp(int x)
        {
            if (board.GetLength(0) - x > 5)
            {
                return false;
            }
            return true;
        }

        static bool checkOverlap(int x, int y)
        {
            var Exist = (from sh in ships
                         from p in sh.sPoints
                         where p.x == x && p.y == y
                         select sh).FirstOrDefault();
            return (Exist == null) ? true : false;
        }


        static string isHit(int x, int y)
        {
            ship Exist = new ship();
            if (board[x, y] != "H" && board[x,y]!= "   |")
            {
                board[x, y] = " H |";
                Exist = (from sh in ships
                from p in sh.sPoints
                where p.x == x && p.y == y
                select sh).FirstOrDefault();

                if (Exist != null)
                {
                    ships.ForEach(a =>
                    {
                        if (a.name == Exist.name)
                        {
                            a.hits++;
                        }
                    });
                    Console.Clear();
                    drawBoard(board);
                }
            }
            
            return (string.IsNullOrEmpty(Exist.name))? "You Missed!" :  Exist.name + " is hit";
        }

        #endregion

    }
}
